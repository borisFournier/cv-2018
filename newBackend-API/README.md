# NewBackend

This project was generated with api-platform (https://api-platform.com/docs/distribution/#using-symfony-flex-and-composer-advanced-users) just by creating entities from a new symfony flex project.

## Development server

Run `bin/console server:run` for a dev server. Navigate to `http://localhost:8000/api` to access to api-platform entities and use CRUD on them. 

Whenever you need to change an entity or a relation between entities, modify your entities in your symfony project, 
use `bin/console doctrine:migrations:diff` to generate a migration file with the differences between your DB and your entities, 
then aplly it using `bin/console doctrine:migrations:migrate`.

To check any change use `php app/console doctrine:migrations:status`.


