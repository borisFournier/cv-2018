<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ExperienceRepository")
 */
class Experience
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date()
     */
    private $startingYear;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date()
     */
    private $endYear;

    /**
     * @ORM\Column(type="text")
     */
    private $missionDetails;

    /**
     * @ORM\Column(type="text")
     */
    private $experienceLogo;

    /**
     * Many experiences have many skills
     * @ORM\ManyToMany(targetEntity="Skill", inversedBy="experiences")
     * @ORM\JoinTable(name="experience_skill")
     */
    private $technos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="experiences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    public function __construct()
    {
        $this->technos = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getMissionDetails(): ?string
    {
        return $this->missionDetails;
    }

    public function setMissionDetails(string $missionDetails): self
    {
        $this->missionDetails = $missionDetails;

        return $this;
    }

    public function getStartingYear(): ?\DateTimeInterface
    {
        return $this->startingYear;
    }

    public function setStartingYear(\DateTimeInterface $startingYear): self
    {
        $this->startingYear = $startingYear;

        return $this;
    }

    public function getEndYear(): ?\DateTimeInterface
    {
        return $this->endYear;
    }

    public function setEndYear(\DateTimeInterface $endYear): self
    {
        $this->endYear = $endYear;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get the value of experienceLogo
     */ 
    public function getExperienceLogo(): ?string
    {
        return $this->experienceLogo;
    }

    /**
     * Set the value of experienceLogo
     *
     * @return  self
     */ 
    public function setExperienceLogo(?string $experienceLogo): self
    {
        $this->experienceLogo = $experienceLogo;

        return $this;
    }

    /**
     * @param Skill $skill
     */
    public function addExperienceSkill(Skill $skill)
    {
        if ($this->technos->contains($skill)) {
            return;
        }
        $this->technos->add($skill);
        $skill->addExperience($this);
    }

    /**
     * @param Skill $skill
     */
    public function removeExperienceSkill(Skill $skill)
    {
        if (!$this->technos->contains($skill)) {
            return;
        }
        $this->technos->removeElement($skill);
        $skill->removeExperience($this);
    }

}
