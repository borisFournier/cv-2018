<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\SkillRepository")
 */
class Skill
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $skillName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $skillPicture;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Assert\Range(
     *      min = 0,
     *      max = 5,
     *      minMessage = "Your level must be at least {{ limit }}",
     *      maxMessage = "Your level cannot be greater than {{ limit }}"
     * )
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="skills")
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    /**
     * Many skills have many experiences
     * @ORM\ManyToMany(targetEntity="Experience", mappedBy="technos")
     */
    private $experiences;

    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->experiences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSkillName(): ?string
    {
        return $this->skillName;
    }

    public function setSkillName(string $skillName): self
    {
        $this->skillName = $skillName;

        return $this;
    }

    public function getSkillPicture(): ?string
    {
        return $this->skillPicture;
    }

    public function setSkillPicture(string $skillPicture): self
    {
        $this->skillPicture = $skillPicture;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @param Experience $experience
     */
    public function addExperience(Experience $experience)
    {
        if ($this->experiences->contains($experience)) {
            return;
        }
        $this->experiences->add($experience);
        $experience->addExperienceSkill($this);
    }

    /**
     * @param Experience $experience
     */
    public function removeExperience(Experience $experience)
    {
        if (!$this->experiences->contains($experience)) {
            return;
        }
        $this->experiences->removeElement($experience);
        $experience->removeExperienceSkill($this);
    }

}
