<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ApiFilter(OrderFilter::class, properties={"id", "obtentionYear"}, arguments={"orderParameterName"="order"})
 * @ORM\Entity(repositoryClass="App\Repository\EducationRepository")
 */
class Education
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="educations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date()
     */
    private $beginingYear;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Date()
     */
    private $obtentionYear;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $school;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank
     */
    private $details;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getSchool(): ?string
    {
        return $this->school;
    }

    public function setSchool(string $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getBeginingYear(): ?\DateTimeInterface
    {
        return $this->beginingYear;
    }

    public function setBeginingYear(\DateTimeInterface $beginingYear): self
    {
        $this->beginingYear = $beginingYear;

        return $this;
    }

    public function getObtentionYear(): ?\DateTimeInterface
    {
        return $this->obtentionYear;
    }

    public function setObtentionYear(\DateTimeInterface $obtentionYear): self
    {
        $this->obtentionYear = $obtentionYear;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}
