-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: cv_boris
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `begining_year` datetime NOT NULL,
  `obtention_year` datetime NOT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DB0A5ED2217BBB47` (`person_id`),
  CONSTRAINT `FK_DB0A5ED2217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (2,1,'Baccalauréat','Albertville','2000-09-03 13:45:30','2001-07-05 13:45:30','Lycée Jean-Moulin','type scientifique'),(3,1,'DUT informatique','Grenoble','2001-09-03 09:27:02','2003-07-03 09:27:02','IUT 2','Diplôme Universitaire de Technologie en Informatique'),(4,1,'Moniteur Educateur (DEME)','Echirolles','2007-09-03 09:27:02','2009-07-03 09:27:02','IFTS','Diplôme d\'Etat de Moniteur Educateur à l\'Institut de Formation des Travailleurs Sociaux'),(5,1,'Analyste Développeur','Grenoble','2017-10-03 09:27:02','2018-04-03 09:27:02','Campus Numérique','Diplôme d\'Etat de Moniteur Éducateur à l\'Institut de Formation des Travailleurs Sociaux');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience`
--

DROP TABLE IF EXISTS `experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `starting_year` datetime NOT NULL,
  `end_year` datetime NOT NULL,
  `mission_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience_logo` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_590C103217BBB47` (`person_id`),
  CONSTRAINT `FK_590C103217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience`
--

LOCK TABLES `experience` WRITE;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` VALUES (1,1,'Moniteur Éducateur','Travailleur social','Les papillons blancs / Les maisons d\'enfants \'Le Chemin\'','Albertville / Grenoble','2005-01-01 09:37:08','2012-12-31 09:37:08','Accueil et prise en charge de personnes (adultes et enfants) en situation de handicap ou présentant des déficiences intellectuelles',''),(2,1,'Expériences professionnelles à l\'étranger','Barman / Serveur / Ouvrier de chantier / Fermier','Restaurant Indien / Mairie de Perth / Ferme de bétails','Darwin / Perth / Adelaide / Melbourne / Alice Spring / Dysart','2013-01-20 09:37:08','2015-03-20 09:37:08','Service bar/restaurant // Chantier du palais de justice de Perth // Ramassage de fruits // Prise en charge du bétails',''),(3,1,'Responsable Bar','Barman / responsable commande','Village vacances \'Les Lavandes\'','Rémuzat','2015-04-01 09:37:08','2015-10-30 09:37:08','Service, commandes, démarchage fournisseurs, nettoyage',''),(4,1,'Vendeur Montagne','Vente','Décatlon','Grenoble','2015-10-20 09:37:08','2016-09-20 09:37:08','Conseils clientèles / Gestions des stocks / Ventes',''),(5,1,'Alternance développeur web','En formation en entreprise','BH Technologies','Grenoble','2017-04-01 09:37:08','2018-04-01 09:37:08','Au sein d\'une équipe de 7 personnes, j\'ai effectué un travail en binôme sur le développement d\'un outil interne au service Web. Cet outil permet la visualisation, édition, ajout suppression de l\'ensemble des catégories/sous-catégories et projets correspondants aux différentes versions en cours de développement ou production. 3 itérations: - développement en Symfony 3 et vue en Twig - Appel d\'Api pour utiliser Angular 2 en front - Utilisation de Docker pour la mise en production',''),(6,1,'Application Web (Le Sou des écoles)','Développeur web (backend / symfony 3)','Projet personnel','Grenoble','2017-10-01 09:37:08','2018-04-30 09:37:08','Projet au sein d\'une équipe de 4 développeurs. Création d\'une application Web pour l\'association du Sou des écoles Stendhal de Voreppe dans le but de gérer l\'organisation d\'évènements. (www.sou-stendhal.fr) Analyse Développement (Authentification, appel d\'Api, BDD MySQL) Automatisation de taches (script shell, Cron) Rencontre et suivi avec le client',''),(7,1,'Iziscar','Développeur Symfony 2.8','Sogevia','Grenoble','2018-12-01 09:37:08','2019-02-28 09:37:08','Travail en équipe de deux développeurs sur la mise en place de docker, la correction d’anomalies et la rédaction d\'une documentation pour un projet de vente de véhicule en ligne. Utilisation des méthodes Agile, communication directe avec le client, utilisation d\'outils de versionning.',''),(8,1,'Online Fitness (formation linguistique)','Développeur Web','NetTest','Grenoble','2018-12-01 09:37:08','2019-02-28 09:37:08','Développements de nouvelles fonctionnalités sur un outil d\'audit linguistique et de tests d\'anglais en ligne.',''),(9,1,'BymyCar / Aphaël','Développeur Web','BymyCar','Grenoble / Chambéry','2019-04-01 09:37:08','2019-06-15 09:37:08','Un premier projet bymycar: - travail sur la partie Back Office du projet - rajouter l\'ajout et la suppression de défauts sur une voiture dans le BO - rafraîchissement d\'une partie de la page (macro) par jquery Une seconde mission Aphael: -projet prestashop -modification rajout de Webservice pour la partie mobile',''),(10,1,'Rothenberg','Développeur','Norsys','Grenoble','2018-11-01 09:37:08','2018-12-15 09:37:08','Travail d\'amélioration continue, de report et de réparation de bugs sur le projet Rothenberg. Rothenberg est un outil open-source qui permet la simplification par l\'utilisation de docker et de makefile de projet symfony ou de bundle symfony.','');
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience_skill`
--

DROP TABLE IF EXISTS `experience_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experience_skill` (
  `experience_id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  PRIMARY KEY (`experience_id`,`skill_id`),
  KEY `IDX_3D6F986146E90E27` (`experience_id`),
  KEY `IDX_3D6F98615585C142` (`skill_id`),
  CONSTRAINT `FK_3D6F986146E90E27` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_3D6F98615585C142` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience_skill`
--

LOCK TABLES `experience_skill` WRITE;
/*!40000 ALTER TABLE `experience_skill` DISABLE KEYS */;
/*!40000 ALTER TABLE `experience_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hobby`
--

DROP TABLE IF EXISTS `hobby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hobby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3964F337217BBB47` (`person_id`),
  CONSTRAINT `FK_3964F337217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hobby`
--

LOCK TABLES `hobby` WRITE;
/*!40000 ALTER TABLE `hobby` DISABLE KEYS */;
INSERT INTO `hobby` VALUES (1,1,'escalade','assets/images/hobbies/escalade.jpg'),(2,1,'badminton','assets/images/hobbies/badminton.jpg'),(3,1,'guitare','assets/images/hobbies/guitare.jpg'),(4,1,'randonnée','assets/images/hobbies/randonnee.jpg'),(5,1,'snowboard','assets/images/hobbies/snowboard.jpg'),(6,1,'squash','assets/images/hobbies/squash.jpg');
/*!40000 ALTER TABLE `hobby` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `langue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D4DB71B5217BBB47` (`person_id`),
  CONSTRAINT `FK_D4DB71B5217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,1,'Anglais','confirmé');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190621124951','2019-06-21 12:51:14'),('20190624122047','2019-06-24 12:22:24');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` int(11) NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` datetime NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'Boris','Fournier','Française','Grenoble',38000,'54 Boulevard Joseph Vallier','00783447808','1982-06-06 23:45:30','Après une carrière dans le social et un voyage en Australie, j\'aimerais allier mes compétences humaines et techniques afin d\'apporter ma pierre à votre édifice. je suis quelqu\'un de curieux, fiable, honnête et persévérant.','bfournier@norsys.fr','assets/images/profil.jpeg','développeur Web');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `skill_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skill_picture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5E3DE477217BBB47` (`person_id`),
  CONSTRAINT `FK_5E3DE477217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,1,'HTML','assets/images/skills/html.png',4),(2,1,'CSS','assets/images/skills/css.png',3),(3,1,'JS','assets/images/skills/javascript.png',3),(4,1,'PHP','assets/images/skills/php.png',4),(5,1,'AngularJS','assets/images/skills/angularJs.png',3),(6,1,'Symfony 3','assets/images/skills/symfony.png',4),(7,1,'Symfony 4','assets/images/skills/symfony-rond.png',4),(8,1,'Angular 2','assets/images/skills/angular2.png',3),(9,1,'Angular 4','assets/images/skills/angular4.jpg',3),(10,1,'Angular 6','assets/images/skills/angular6.jpg',3),(12,1,'Docker','assets/images/skills/docker.jpg',3),(13,1,'GIT','assets/images/skills/git.jpg',4),(14,1,'MySQL','assets/images/skills/mysql.jpg',4),(15,1,'JQuery','assets/images/skills/jquery.png',3),(16,1,'Linux','assets/images/skills/linux.png',4),(17,1,'PhpUnit','assets/images/skills/phpunit.png',2),(18,1,'Prestashop','assets/images/skills/prestashop.png',2),(19,1,'Twig','assets/images/skills/twig.png',4),(20,1,'Ubuntu','assets/images/skills/ubuntu.png',4);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-25  9:17:50
