export class Hobby {
    id: number;
    name: string;
    picture: string;
    personID: number;
}
