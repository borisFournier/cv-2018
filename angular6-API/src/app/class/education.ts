import { Person } from "./person";

export class Education {
    id: number;
    name: string;
    city: string;
    school: string;
    details: string;
    beginingYear: Date;
    obtentionYear: Text;
    personID: number;
}

