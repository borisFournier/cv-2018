import { Person } from "./person";

export class Skill {
    id: number;
    skillName: string;
    skillPicture: string;
    level: number;
    personID: number;
    experienceID: number;
}
