import { Language } from "./language";
import { Skill } from "./skill";
import { Education } from "./education";

export class Person {
    id: number;
    firstname: string;
    lastname: string;
    nationality: string;
    city: string;
    street: string;
    postcode: number;
    phone: string;
    dateOfBirth: Date;
    description: Text;
    email: string;
    profilPicture: string;
    jobTitle: string;
    languages: Language[];
    skills: Skill[];
    eductions: Education[];

}
