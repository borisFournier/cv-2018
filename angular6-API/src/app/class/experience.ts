import { Skill } from "./skill";

export class Experience {
    id: number;
    title: string;
    role: string;
    company: string;
    city: string;
    missionDetails: string;
    startingYear: Date;
    endYear: Date;
    personID: number;
    experienceLogo: string;
    technos: Skill[];
}
