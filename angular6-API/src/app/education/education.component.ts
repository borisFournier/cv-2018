import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';
import { Education } from '../class/education';



@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

  person:Person;
  educations:Education[];

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  ngOnInit() {
    this.getEducationsByPerson();
    this.getPerson();
  }

  getEducationsByPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetEducationsByPerson(id)
    .subscribe(
      data => {
        this.educations = data;
        //console.log(data);
      }
      );
  }

  getPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetPerson(id)
    .subscribe(
      data => {
        this.person = data;
        //console.log(data);
      }
      );
  }

  goBack(): void {
    this.location.back();
  }

}
