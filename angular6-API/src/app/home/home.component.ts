import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  persons:Person[];


  constructor(private apiService:ApiService) { }

  

  ngOnInit() {

    this.getPersons();
  }

  getPersons():void{
    this.apiService.apiGetPersons()
    .subscribe(
      data => {
        this.persons = data;
        console.log(data);
      }
      );
    
  }

}
