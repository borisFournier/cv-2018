import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit {

  person: Person;

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
    ) { }

  ngOnInit() {
    this.getPerson();
  }

  getPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetPerson(id)
    .subscribe(
      data => {
        this.person = data;
        //console.log(data);
      }
      );
  }

  goBack(): void {
    this.location.back();
  }

  update(): void {
    this.apiService.apiUpdatePerson(this.person)
    .subscribe(() => this.goBack());
  }
}
