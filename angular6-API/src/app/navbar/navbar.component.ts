import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() person: Person;

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  ngOnInit() {
  }
}
