import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Skill } from '../class/skill';

@Component({
  selector: 'app-canvas-chart',
  templateUrl: './canvas-chart.component.html',
  styleUrls: ['./canvas-chart.component.css']
})
export class CanvasChartComponent implements OnInit {

  @Input() skill: Skill;

  //pie chart 
  public pieChartLabels:string[] = [];
  public pieChartData:number[] = [];
  public pieChartType:string = 'pie';
  public pieChartOptions:any = {'backgroundColor': [
    "#FF6384"]}
 
  // events on slice click
  public chartClicked(e:any):void {
    console.log(e);
  }
  // event on pie chart slice hover
  public chartHovered(e:any):void {
    console.log(e);
  }

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  ngOnInit() {
    this.getValueChart();
  }

  getValueChart():void{
    this.pieChartData.push(this.skill.level);
    this.pieChartLabels.push(this.skill.skillName);

  }

}
