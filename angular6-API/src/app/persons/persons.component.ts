import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.css']
})
export class PersonsComponent implements OnInit {

  persons:Person[];

  constructor(private apiService:ApiService) { }

  ngOnInit() {
    this.getPersons();
  }

  getPersons():void{
    this.apiService.apiGetPersons()
    .subscribe(
      data => {
        this.persons = data;
        //console.log(data);
      }
      );
    
  }

// addPerson():void{
//   this.apiService.apiAddPerson()
//   .subscribe(
//     data => {
//       this.person = data;
//       console.log(data);
//     }
//   )
// }

}
