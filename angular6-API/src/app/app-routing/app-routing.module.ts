import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { PersonDetailComponent } from '../person-detail/person-detail.component';
import { SkillsComponent } from '../skills/skills.component';
import { ExperiencesComponent } from '../experiences/experiences.component';
import { HobbiesComponent } from '../hobbies/hobbies.component';
import { EducationComponent } from '../education/education.component';
import { PersonsComponent } from '../persons/persons.component';


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'detail/:id', component: PersonDetailComponent},
  {path: 'persons', component: PersonsComponent},
  {path: 'detail/:id/education', component: EducationComponent},
  {path: 'detail/:id/skills', component: SkillsComponent},
  {path: 'detail/:id/experiences', component: ExperiencesComponent},
  {path: 'detail/:id/hobbies', component: HobbiesComponent},
  { path: '**', redirectTo: '/home' } //to redirect all path unknown
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
