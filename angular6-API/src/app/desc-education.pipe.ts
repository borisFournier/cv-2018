import { Pipe, PipeTransform } from '@angular/core';
import { Education } from './class/education';

@Pipe({
  name: 'descEducation'
})
export class DescEducationPipe implements PipeTransform {

  transform(allExperiences: Education[]): Education[] {

    if (!Array.isArray(allExperiences)) {
      return;
    }
    allExperiences.sort((a , b) => { 
      return b.beginingYear > a.beginingYear ? 1: -1
    });
    return allExperiences;
  }

}
