import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';
import { Experience } from '../class/experience';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.css']
})
export class ExperiencesComponent implements OnInit {

  person:Person;
  experiences:Experience[];

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  ngOnInit() {
    this.getExperiencesByPerson();
    this.getPerson();
  }

  getExperiencesByPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetExperiencesByPerson(id)
    .subscribe(
      data => {
        this.experiences = data;
        //console.log(data);
      }
      );
  }

  getPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetPerson(id)
    .subscribe(
      data => {
        this.person = data;
        //console.log(data);
      }
      );
  }

  goBack(): void {
    this.location.back();
  }

}
