import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppRoutingModule } from './app-routing/app-routing.module';

import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MessagesComponent } from './messages/messages.component';
import { PersonDetailComponent } from './person-detail/person-detail.component';
import { PersonsComponent } from './persons/persons.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SkillsComponent } from './skills/skills.component';
import { HobbiesComponent } from './hobbies/hobbies.component';
import { EducationComponent } from './education/education.component';
import { ExperiencesComponent } from './experiences/experiences.component';
import { CanvasChartComponent } from './canvas-chart/canvas-chart.component';
import { DescExperiencesPipe } from './desc-experiences.pipe';
import { DescEducationPipe } from './desc-education.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MessagesComponent,
    PersonDetailComponent,
    PersonsComponent,
    NavbarComponent,
    SkillsComponent,
    HobbiesComponent,
    EducationComponent,
    ExperiencesComponent,
    CanvasChartComponent,
    DescExperiencesPipe,
    DescEducationPipe,
  ],
  imports: [
    BrowserModule,    // import HttpClientModule after BrowserModule.
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
