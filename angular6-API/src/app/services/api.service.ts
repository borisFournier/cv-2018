import { Injectable } from '@angular/core';
import { Person } from '../class/person';
import { Skill } from '../class/skill';
import { Experience } from '../class/experience';
import { Education } from '../class/education';
import { Hobby } from '../class/hobby';
import { Language } from '../class/language';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';


const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json'
  })
};

const getOptions = {
  headers: new HttpHeaders({ 
    'accept': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})


export class ApiService {

  private api ='http://localhost:8000/api';
  

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  


  /*************************************************************************************
   *PERSON
   *************************************************************************************/
  apiGetPersons(): Observable<Person[]> {
    let apiPerson = this.api + '/people';
    return this.http.get<Person[]>(apiPerson,getOptions)
    .pipe(
    tap(persons => this.log('fetched persons')),
    catchError(this.handleError('getPersons', []))
    );
  }

  apiGetPerson(id: number): Observable<Person> {
    const apiPerson = this.api + '/people' + `/${id}`;
    return this.http.get<Person>(apiPerson,getOptions)
    .pipe(
    tap(person => this.log(`fetched person id=${id}`)),
    catchError(this.handleError<Person>(`getPerson id=${id}`))
    );
  }

  apiUpdatePerson(person: Person): Observable<any> {
    let apiPerson = this.api + '/people';
    return this.http.put(apiPerson, person, httpOptions)
    .pipe(
      tap(_ => this.log(`updated person id = ${person.id}`)),
      catchError(this.handleError<any>('updatedHero'))
    );
  }


  /*************************************************************************************
   *Experience
   *************************************************************************************/ 
  apiGetExperiencesByPerson(idPerson : number): Observable<Experience[]> {
    let apiExperience = this.api + '/people' + `/${idPerson}` + '/experiences?order[obtentionYear]=desc';
    return this.http.get<Experience[]>(apiExperience,getOptions)
    .pipe(
    tap(experiences => this.log('fetched experiences by person')),
    catchError(this.handleError('getExperiencesByPerson', []))
    );
  }
   

  /*************************************************************************************
   *Education
   *************************************************************************************/
  apiGetEducationsByPerson(idPerson : number): Observable<Education[]> {
    let apiEducation = this.api + '/people' + `/${idPerson}` + '/educations?sort_by=beginingYear&order_by=asc';
    return this.http.get<Education[]>(apiEducation,getOptions)
    .pipe(
    tap(experiences => this.log('fetched educations by person')),
    catchError(this.handleError('getEducationsByPerson', []))
    );
  }

  /*************************************************************************************
   *SKILL
   *************************************************************************************/
  apiGetSkills(): Observable<Skill[]> {
    let apiSkill = this.api + '/skills';
    return this.http.get<Skill[]>(apiSkill,getOptions)
    .pipe(
    tap(skills => this.log('fetched skills')),
    catchError(this.handleError('getSkills', []))
    );
  }

  apiGetSkillsByPerson(idPerson : number): Observable<Skill[]> {
    let apiSkill = this.api + '/people' + `/${idPerson}` + '/skills';
    return this.http.get<Skill[]>(apiSkill,getOptions)
    .pipe(
    tap(skills => this.log('fetched skills by person')),
    catchError(this.handleError('getSkillsByPerson', []))
    );
  }

  /*************************************************************************************
   *LANGUAGE
   *************************************************************************************/
  apiGetLanguagesByPerson(idPerson : number): Observable<Language[]> {
    let apiLanguage = this.api + '/people' + `/${idPerson}` + '/skills';
    return this.http.get<Language[]>(apiLanguage,getOptions)
    .pipe(
    tap(languages => this.log('fetched languages by person')),
    catchError(this.handleError('getLanguagesByPerson', []))
    );
  }

  /*************************************************************************************
   *HOBBY
   *************************************************************************************/
  apiGetHobbiesByPerson(idPerson : number): Observable<Hobby[]> {
    let apiHobby = this.api + '/people' + `/${idPerson}` + '/hobbies';
    return this.http.get<Hobby[]>(apiHobby,getOptions)
    .pipe(
    tap(hobbies => this.log('fetched hobbies by person')),
    catchError(this.handleError('gethobbiesByPerson', []))
    );
  }


  /*************************************************************************************
   *ERROR
   *************************************************************************************/

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a ApiService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`ApiService: ${message}`);
  }
}
