import { Pipe, PipeTransform } from '@angular/core';
import { Experience } from './class/experience';

@Pipe({
  name: 'descExperiences'
})
export class DescExperiencesPipe implements PipeTransform {

  transform(allExperiences: Experience[]): Experience[] {

    if (!Array.isArray(allExperiences)) {
      return;
    }
    allExperiences.sort((a , b) => { 
      return b.startingYear > a.startingYear ? 1: -1
    });
    return allExperiences;
  }
  
}
