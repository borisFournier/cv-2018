import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';
import { Skill } from '../class/skill';
//import { chart } from 'chart.js';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {

  person:Person;
  skills:Skill[];

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  ngOnInit() {
    this.getSkillsByPerson();
    this.getPerson();
  }

  getSkillsByPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetSkillsByPerson(id)
    .subscribe(
      data => {
        this.skills = data;
        // console.log(data);
        //  data.forEach(item => {
        //    for(let i=0; i<item.level;i++){
        //     console.log(item.nbStars);
        //     item.nbStars.push(i);
        //     console.log(item.nbStars);

        //    }
        //  })
      }
      );
  }

  public generateStarsArray(level: number):Array<any>{
    let stars = [];
    for(let i=0; i < level; i++){
      stars.push(i);
    }
    return stars;
  }

  getPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetPerson(id)
    .subscribe(
      data => {
        this.person = data;
      }
      );
  }

  goBack(): void {
    this.location.back();
  }

  

}
