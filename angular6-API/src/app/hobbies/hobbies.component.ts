import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '../services/api.service';
import { Person } from '../class/person';
import { Hobby } from '../class/hobby';

@Component({
  selector: 'app-hobbies',
  templateUrl: './hobbies.component.html',
  styleUrls: ['./hobbies.component.css']
})
export class HobbiesComponent implements OnInit {

  person:Person;
  hobbies:Hobby[];

  constructor(
    private apiService:ApiService,
    private route:ActivatedRoute,
    private location:Location
  ) { }

  ngOnInit() {
    this.getHobbiesByPerson();
    this.getPerson();
  }

  getHobbiesByPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetHobbiesByPerson(id)
    .subscribe(
      data => {
        this.hobbies = data;
        //console.log(data);
      }
      );
  }

  getPerson():void{
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.apiGetPerson(id)
    .subscribe(
      data => {
        this.person = data;
        //console.log(data);
      }
      );
  }

  goBack(): void {
    this.location.back();
  }
}
